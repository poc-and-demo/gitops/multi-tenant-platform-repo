# multi-tenant-platform-repo

## Roles

**Platform Admin**

  - Has cluster admin access to the fleet of clusters
  - Has maintainer access to the fleet Git repository
  - Manages cluster wide resources (CRDs, controllers, cluster roles, etc)
  - Onboards the tenant’s main GitRepository and Kustomization
  - Manages tenants by assigning namespaces, service accounts and role binding to the tenant's apps

**Tenant**

  - Has admin access to the namespaces assigned to them by the platform admin
  - Has maintainer access to the tenant Git repository and apps repositories
  - Manages app deployments with GitRepositories and Kustomizations
  - Manages app releases with HelmRepositories and HelmReleases
